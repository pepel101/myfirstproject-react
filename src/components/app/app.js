 import { Component } from "react";


import AppInfo from "../app-info/app-info";
import SearchPanel from "../search-panel/search-panel";
import AppFilter from "../app-filter/app-filter";
import EmployersList from "../employers-list/employers-list";
import EmployersAddForm from "../emloyers-add-form/employers-add-form";
import "./app.css";

class App extends Component{
        constructor(props){
        super(props);
        this.state={
            data: [
                {name: "John C." , salary: 340 , increase:false ,rise:true, id: 1 },
                {name: "Mat V." , salary: 1500 ,increase:true,rise:false,  id: 2 },
                {name: "Ivan B." , salary: 2500 ,increase:false,rise:false,  id: 3 },
            
            ],
            term:'',
            filter: 'rise'
        }
        this.maxId = 4;
    }
      deleteItem=(id)=>{
          this.setState(({data})=>{
               
            //  const index = data.findIndex(elem=> elem.id===id);
            //  const before= data.slice(0, index);
            //  const after = data.slice(index + 1)

            //  const newArr = [...before, ...after];

             return{
                 data: data.filter(item=>item.id!==id)
             }
                
          })
      }
      addUser = (name, salary) => {
          const newUser ={
              name,
              salary,
              increase:false,
              rise:false,
              id:this.maxId++}
              this.setState(({data})=>{
                  const newArrUser =[...data,newUser];
                  return{
                      data: newArrUser
                  }
              })
          }
    onToggleProp=(id,prop)=>{
           
            // this.setState(({data})=>{
            //     const index = data.findIndex(elem=>elem.id===id);// получаем индекс жлемента  с которым работаем
            //     const old = data[index];
            //     const newItem={...old,increase:!old.increase }
            //     const newArr=[...data.slice(0,index), newItem, ...data.slice(index+1)]
            //     return{
            //         data:newArr
            //     }
            // })
            this.setState(({data})=>({
               data:data.map(item=>{
                if(item.id===id){//если совпали айдишики  то возвращаем новый обьект
                    return{...item,[prop]:!item[prop]}// содержит эту сущность всю(старые свойства  и  нвоые )
                }
                return item;

               })
            }))
            //возвращаем  новый оъект с новым date через колбекфункцию внутри
          }
          //Метод для  поиска  сотрудника!! НАЧАЛО
          searchEmp =(items,term)=>{
            if (term.length===0){
                return items;
            }

            return items.filter(item=>{
                return item.name.indexOf(term)>-1 
                
            })
          }

          //МЕТОД ДЛЯ  ПОИСКА!!!! КОНЕЦ

          // Метод для передачи  состояния  из seach-panel.js в app.js!!! НАЧАЛО
        onUpdateSearch=(term)=>{
            this.setState({term});
        }
          // Метод для передачи  состояния  из seach-panel.js в app.js!!! КОНЕЦ

        //МЕТОД ДЛЯ  ФИЛЬТРАЦИИ  СОТРУДНИКОВ!!! НАЧАЛО
        filterPost=(items,filter)=>{
            switch(filter){
                case "rise":
                    return items.filter(item=>item.rise);
                case "moreThen1000":
                    return items.filter(item=>item.salary>1000)
                    default:
                        return items

            }

        }

        //МЕТОД ДЛЯ  ФИЛЬТРАЦИИ  СОТРУДНИКОВ!!! КОНЕЦ

        //МЕТОД ДЛЯ ПЕРЕДАЧИ  КНОПКИ  СОСТОЯНИЯ!!!! НАЧАЛО
        onFilterSelect=(filter)=>{
            this.setState({filter});

        }
        //МЕТОД ДЛЯ ПЕРЕДАЧИ  КНОПКИ  СОСТОЯНИЯ!!!! КОНЕЦ
          //   onToggleRise=(id)=>{
        //       console.log(`rise this ${id}`)    
        //       this.setState(({data})=>({
        //         data:data.map(item=>{
        //             if(item.id===id){
        //                 return{...item,rise:!item.rise}
        //             }
        //             return item;
        //         })

        //       }))       
        //   }
      
      //передается до самого нижнего уровня и вызывается  только по клику кнопки на которую повешено событие
   render() {
       const {data, term ,filter}=this.state
       const emploeeys=this.state.data.length;
       const increased = this.state.data.filter(item=>item.increase).length;
       const visibleDate = this.filterPost(this.searchEmp(data,term), filter);
    return(
        <div className =" app">
            <AppInfo
        emploeeys={emploeeys}
        increased={increased}  />
            <div className='search-panel'>
            <SearchPanel
            onUpdateSearch={this.onUpdateSearch}/>
            <AppFilter
            filter={filter}
            onFilterSelect={this.onFilterSelect}/>

            </div>
            <EmployersList
            data ={visibleDate}
            onDelete={this.deleteItem} 
            onToggleProp={this.onToggleProp}
            />
            <EmployersAddForm onAdd={this.addUser}/>

        </div>

     )
   }
}

 export default App;